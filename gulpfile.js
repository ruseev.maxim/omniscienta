'use strict';

let gulp = require('gulp'),
  debug = require('gulp-debug'),
  uglifyJs = require('gulp-uglify'),
  uglifyCss = require('gulp-minify-css'),
  concat = require('gulp-concat'),
  watch = require('gulp-watch'),
  copy = require('gulp-copy'),
  strip = require('gulp-strip-comments'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  pug = require('gulp-pug'),
  templateCache = require('gulp-angular-templatecache'),
  autoprefixeer = require('gulp-autoprefixer'),
  babel = require('gulp-babel');


gulp.task('scss', () => {
  return gulp.src('src/assets/styles/scss/bootstrap.scss')
    .pipe(sass()).on('error', sass.logError)
    .pipe(autoprefixeer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(uglifyCss())
    .pipe(concat('custom.min.css'))
    .pipe(gulp.dest('src/assets/styles/css/'));
});
