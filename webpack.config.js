const path = require('path');

module.exports = {
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  entry: './src/index.jsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'src'),
  },
  module: {
    rules: [{
      test: /\.scss$/,
      use: [{
        loader: 'style-loader',
      }, {
        loader: 'css-loader', options: {
          sourceMap: true
        }
      }, {
        loader: 'sass-loader', options: {
          sourceMap: true
        }
      }, {
        loader: 'sass-resources-loader',
        options: {
          resources: [
            './src/assets/styles/scss/_fonts.scss',
            './src/assets/styles/scss/_functions.scss',
            './src/assets/styles/scss/_variables.scss',
            './src/assets/styles/scss/_print.scss',
          ],
        }
      }],
    }, {
      test: /\.jsx$/,
      exclude: [/node_modules/],
      use: [{
        loader: 'babel-loader',
        options: {presets: ['react', 'es2015']},
      }],
    }],
  },
  devServer: {
    contentBase: path.join(__dirname, '/src'),
    historyApiFallback: true,
    compress: true,
    port: 7000,
  },
  devtool: "source-map"
};
