function showGetAuditDialog(title = null, description = null) {
  return {
    type: 'SHOW_GET_AUDIT_DIALOG',
    title,
    description,
    show: true,
  }
}

function hideGetAuditDialog() {
  return {
    type: 'HIDE_GET_AUDIT_DIALOG',
    show: false,
  }
}

export {
  showGetAuditDialog,
  hideGetAuditDialog,
}