function showAlert(title, content, ok = 'ок') {
  return {
    type: 'SHOW_ALERT',
    show: true,
    title: title,
    content: content,
    ok: ok,
  }
}

function hideAlert() {
  return {
    type: 'HIDE_ALERT',
  }
}

export {
  showAlert,
  hideAlert
}