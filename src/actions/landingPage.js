function sendNewLead(resolve, reject, values) {

  return {
    type: 'SEND_NEW_LEAD',
    payload: {
      resolve,
      reject,
      values,
    }
  }
}

export {
  sendNewLead,
}