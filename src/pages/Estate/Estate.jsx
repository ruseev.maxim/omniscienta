import React from 'react';
import ReactGA from 'react-ga';

import Toolbar from './../../blocks/Toolbar/Toolbar';

const Estate = () => {

  ReactGA.pageview(window.location.pathname + window.location.search);

  let slides = [];
  for(let i = 1; i <= 25; i++) {
    slides.push(`Estate 2.0${('0' + i).slice(-2)}.png`)
  }

  return (
    <div>
      <Toolbar />
      {slides.map((slide, i) => {
        return <img src={`/assets/images/estate/${slide}`} width='100%' key={i} />
      })}
    </div>
  )
};

export default Estate;
