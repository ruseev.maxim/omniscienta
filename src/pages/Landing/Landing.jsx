import React from 'react';

import ReactGA from 'react-ga';

// blocks
import Toolbar from './../../blocks/Toolbar/Toolbar';
import Descriptor from './blocks/Descriptor/Descriptor';
import Technologies from './blocks/Technologies/Technologies';
import QuickContact from './blocks/QuickContact/QuickContact';
import Analytics from './blocks/Analytics/Analytics';
import Prices from './blocks/Prices/Prices';
import Capabilities from './blocks/Capabilities/Capabilities';
import OurClients from './blocks/OurClients/OurClients';
import WorkTogether from './blocks/WorkTogether/WorkTogether';
import Map from './blocks/Map/Map';
import Footer from './../../blocks/Footer/Footer';


const Landing = () => {

  ReactGA.pageview(window.location.pathname + window.location.search);

  return (
    <div>
        <Toolbar/>
        <Descriptor/>
        <Technologies/>
        <QuickContact formName="quickContactBlock1"/>
        <Analytics/>
        <Prices/>
        <QuickContact formName="quickContactBlock2"/>
        <Capabilities/>
        <OurClients/>
        <WorkTogether/>
        <Map/>
        <Footer/>
    </div>)
};

export default Landing;

