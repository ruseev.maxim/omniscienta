import React from 'react';

import { connect } from 'react-redux';

import { isElementInTheCenter } from './../../../../components/modules/calculateElementPosition';

import ReactGA from 'react-ga';

import t from './../../../../modules/i18n/i18n';

const bubbles = [
  {
    id: 1,
    icon: 'icon.svg',
    headerHtml: 'Programmatic',
    descriptionHtml: t('TECHNOLOGIES_PROGRAMMATIC'),
  }, {
    id: 2,
    icon: 'icon (1).svg',
    headerHtml: 'Social Marketing',
    descriptionHtml: t('TECHNOLOGIES_SOCIAL_MARKETING'),
  }, {
    id: 3,
    icon: 'icon (2).svg',
    headerHtml: t('TECHNOLOGIES_CONTEXT_TITLE'),
    descriptionHtml: t('TECHNOLOGIES_CONTEXT_DESCRIPTION'),
  }, {
    id: 4,
    icon: 'icon (3).svg',
    headerHtml: t('TECHNOLOGIES_RETARGETING_TITLE'),
    descriptionHtml: t('TECHNOLOGIES_RETARGETING_DESCRIPTION'),
  }, {
    id: 5,
    icon: 'triangle.svg',
    headerHtml: t('TECHNOLOGIES_TAKE_A_LOOK_TITLE'),
    descriptionHtml: t('TECHNOLOGIES_TAKE_A_LOOK_DESCRIPTION'),
  }
];

const bubbleItemsDesktop = bubbles.map(bubble =>
  <div key={bubble.id}
       className={`diagram__bubble-container
       diagram__bubble-container-id${bubble.id}`}>
    <div className="row align-items-center">
      <a className={`diagram__bubble-container-bubble ${bubble.id === 5 ? 'diagram__bubble-container-bubble-video-play' : ''}`}
         target="_blank" href="https://www.youtube.com/watch?v=V2pdJvxoge4&feature=youtu.be">
        <img src={`/assets/icons/${bubble.icon}`}/>
      </a>
      <div>
        <h2>{bubble.headerHtml}</h2>
        <span dangerouslySetInnerHTML={{__html: bubble.descriptionHtml}}/>
      </div>
    </div>
  </div>
);

const bubbleItemsMobile = bubbles.map(bubble => {
  if(bubble.id !== 5)return (
    <div key={bubble.id} className="row diagram__bubble-container-mobile">
      <div className="col text-center">
        <img src={`/assets/icons/${bubble.icon}`}/>
      </div>

      <div className="w-100"/>

      <div className="col text-center">
        <h2>{bubble.headerHtml}</h2>
        <span dangerouslySetInnerHTML={{__html: bubble.descriptionHtml}}/>
      </div>
    </div>
  );
});


const Technologies = props => {

  const triggerElement = document.querySelector('.diagram__bubble-container-id1');
  if(triggerElement && props.pageYOffset) {
    if(isElementInTheCenter(triggerElement, props.pageYOffset)) {
      ReactGA.event({
        category: 'scroll',
        action: '2'
      });
    }
  }

  return (
    <div>
      <div className="technologies">
        <div className="container">

          <div className="row">
            <div className="col text-center">
              <h1 dangerouslySetInnerHTML={{__html: t('TECHNOLOGIES_TITLE')}} />
              <h3 className="text-omni-gray-1">{t('TECHNOLOGIES_DESCRIPTION')}</h3>
            </div>
          </div>
        </div>
        <div className="container diagram-container d-none d-lg-block">
          <div className="diagram">

            <div className="">
              <div className="diagram__oval_1"/>
              <div className="diagram__mdp">
                <div className="row">
                  <div className="col">
                    <img src="/assets/icons/icon_mdp.svg"/>
                    <h2>Marketing Data Platform</h2>
                    <p dangerouslySetInnerHTML={{__html: t('TECHNOLOGIES_MARKETING_DATA_PLATFORM')}} />
                  </div>
                </div>
              </div>
              {bubbleItemsDesktop}
            </div>
          </div>
        </div>

        <div className="container d-xs-block d-sm-block d-md-block d-lg-none">
          <div className="diagram">
            <div className="">
              <div className="row diagram__bubble-container-mobile">
                <div className="col text-center">
                  <img src="/assets/icons/icon_mdp.svg"/>
                </div>

                <div className="w-100"/>

                <div className="col text-center">
                  <h2>Marketing Data Platform</h2>
                  <span>MDP – подраздел DMP. Платформа по управлению данными с помощью Data Science. Максимальная персонализация рекламы под каждого пользователя в любом рекламном канале.</span>
                </div>
              </div>

              {bubbleItemsMobile}

              <div className="row diagram__bubble-container-mobile">
                <div className="col text-center">
                  <img src="/assets/icons/video_play.svg"/>
                </div>

                <div className="w-100"/>

                <div className="col text-center">
                  <h2>Взгляд изнутри</h2>
                  <span>Посмотрите как мы работаем</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="divider"/>
    </div>
  )
};

const mapStateToProps = state => ({
  pageYOffset: state.scroll.pageYOffset
});

export default connect(mapStateToProps)(Technologies);