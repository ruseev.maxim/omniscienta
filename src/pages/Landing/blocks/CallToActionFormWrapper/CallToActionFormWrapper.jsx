import React from 'react';

// actions
import { sendNewLead } from './../../../../actions/landingPage';

// form
import CallToActionForm from './../../../../components/Forms/CallToActionForm';

// prop-types
import PropTypes from 'prop-types';

// redux
import { connect } from 'react-redux';

let CallToActionFormWrapper = props => {
  const { formName, handleSubmit } = props;

  return (
    <CallToActionForm formName={formName} onSubmit={handleSubmit} />
  );
};

CallToActionFormWrapper.propTypes = {
  formName: PropTypes.string.isRequired
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: values => {
    return new Promise((resolve, reject) => {
      dispatch(sendNewLead(resolve, reject, values));
    })
  }
});

export default connect(null, mapDispatchToProps)(CallToActionFormWrapper);