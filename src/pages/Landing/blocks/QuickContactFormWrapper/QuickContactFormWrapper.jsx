import React from 'react';

// actions
import { sendNewLead } from './../../../../actions/landingPage';

// form
import QuickContactForm from './../../../../components/Forms/QuickContactForm';

// prop-types
import PropTypes from 'prop-types';

// redux
import { connect } from 'react-redux';

let QuickContactFormWrapper = props => {

  const { formName, handleSubmit } = props;

  return (
    <QuickContactForm formName={formName} onSubmit={handleSubmit}/>
  )
};

QuickContactFormWrapper.propTypes = {
  formName: PropTypes.string.isRequired
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: values => {
    return new Promise((resolve, reject) => {
      dispatch(sendNewLead(resolve, reject, values));
    })
  }
});

export default connect(null, mapDispatchToProps)(QuickContactFormWrapper);
