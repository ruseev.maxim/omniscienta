import React from 'react';

// redux
import { connect } from 'react-redux';

import { isElementInTheCenter } from './../../../../components/modules/calculateElementPosition';

import ReactGA from 'react-ga';

// actions
import { showGetAuditDialog } from './../../../../actions/getAudit';

import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';

import t from './../../../../modules/i18n/i18n';
// import CallToAction from "../CallToAction/CallToAction";
// class Prices extends React.Component {
//   constructor() {
//     super();
//
//     this.state = {
//       modal: false,
//     };
//
//     this.toggle = this.toggle.bind(this);
//   }
//
//   toggle() {
//     this.setState({
//       modal: !this.state.modal
//     });
//   }
//
//   render() {
//   }
// }

class Prices extends React.Component  {

  constructor(props) {
    super(props);
    this.state = { swipeIsVisible: true};
    this.onSwipeLayoutClick = this.onSwipeLayoutClick.bind(this);
  }

  onSwipeLayoutClick() {
    this.setState({ swipeIsVisible: false });
  }


  render() {
  const { showGetAuditDialog, pageYOffset } = this.props;
  const { swipeIsVisible } = this.state;

  const triggerElement = document.querySelector('.prices-table-container');
  if(triggerElement && pageYOffset) {
    if(isElementInTheCenter(triggerElement, pageYOffset)) {
      ReactGA.event({
        category: 'scroll',
        action: '4'
      });
    }
  }
    return(
      <div>
        <div className="prices">
          <div className="container">

            <div className="row">
              <div className="col text-center">
                <h1>{t('PRICES_TITLE')}</h1>
                <h3 className="text-omni-gray-1">{t('PRICES_DESCRIPTION')}</h3>
              </div>
            </div>

            <div className="row justify-content-center prices-table-container">
              {swipeIsVisible ? <div className="swipe-overlay" onClick={this.onSwipeLayoutClick} onTouchMove={this.onSwipeLayoutClick}>
                <img className="swipe-overlay__swipe-icon" src="/assets/icons/swipe.svg" />
              </div> : null}

              <div className="col-xs-auto prices-table-container__col">
                <table className="table prices-table">
                  <thead>
                  <tr>
                    <th></th>
                    <th>{t('PRICES_BASE')}</th>
                    <th>{t('PRICES_EXPAND')}</th>
                    <th>{t('PRICES_ADVANCED')}</th>
                    <th>{t('PRICES_MAX')}</th>
                  </tr>
                  </thead>

                  <tbody>
                  <tr>
                    <td className="prices-table__horizontal-name">{t('PRICES_BUDGET')}</td>
                    <td>{t('PRICE_BASE_AMOUNT')}</td>
                    <td>{t('PRICE_EXPAND_AMOUNT')}</td>
                    <td>{t('PRICE_ADVANCED_AMOUNT')}</td>
                    <td>{t('PRICE_MAX_AMOUNT')}</td>
                  </tr>

                  <tr>
                    <td className="prices-table__horizontal-name">{t('PRICES_CHANNELS')}</td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                  </tr>

                  <tr>
                    <td className="prices-table__horizontal-name">{t('PRICES_COMMISSION')}</td>
                    <td>30%</td>
                    <td>25%</td>
                    <td>25%</td>
                    <td>20%</td>
                  </tr>

                  <tr>
                    <td className="prices-table__horizontal-name">{t('PRICES_ANALYTICS')}</td>
                    <td>-</td>
                    <td>+</td>
                    <td>+</td>
                    <td>+</td>
                  </tr>

                  <tr>
                    <td className="prices-table__horizontal-name">{t('PRICES_PERSONALISATION')}</td>
                    <td>+</td>
                    <td>+</td>
                    <td>+</td>
                    <td>+</td>
                  </tr>

                  <tr>
                    <td className="prices-table__horizontal-name">{t('PRICES_COURSE')}</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>+</td>
                  </tr>

                  <tr>
                    <td className="prices-table__horizontal-name">{t('PRICES_MANAGER')}</td>
                    <td>+</td>
                    <td>+</td>
                    <td>+</td>
                    <td>+</td>
                  </tr>

                  <tr>
                    <td className="prices-table__horizontal-name">{t('PRICES_INFO')}</td>
                    <td>+</td>
                    <td>+</td>
                    <td>+</td>
                    <td>+</td>
                  </tr>

                  <tr>
                    <td/>
                    <td>
                      <Button onClick={showGetAuditDialog(t('PRICES_CHOOSE_BASE'))} outline className="descriptor__form-call-to-action" color="primary">{t('PRICES_CHOOSE_BUTTON')}</Button>
                    </td>
                    <td>
                      <Button onClick={showGetAuditDialog(t('PRICES_CHOOSE_EXPAND'))} outline className="descriptor__form-call-to-action" color="primary">{t('PRICES_CHOOSE_BUTTON')}</Button>
                    </td>
                    <td>
                      <Button onClick={showGetAuditDialog(t('PRICES_CHOOSE_ADVANCED'))} outline className="descriptor__form-call-to-action" color="primary">{t('PRICES_CHOOSE_BUTTON')}</Button>
                    </td>
                    <td>
                      <Button onClick={showGetAuditDialog(t('PRICES_CHOOSE_MAX'))} outline className="descriptor__form-call-to-action" color="primary">{t('PRICES_CHOOSE_BUTTON')}</Button>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
        <div className="divider" />
      </div>
    )
  }




}

const mapStateToProps = state => ({
  pageYOffset: state.scroll.pageYOffset
});

const mapDispatchToProps = dispatch => {
  const defaultDescription = 'Вместе с консультацией по тарифу вам будет предоставлен<br/>бесплатный аудит сайта и рекомендации от аналитиков';
  return {
    showGetAuditDialog: (title) => () => dispatch(showGetAuditDialog(title, defaultDescription))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Prices);