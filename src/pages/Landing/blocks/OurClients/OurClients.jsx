import React from 'react';

import {t, getLang} from './../../../../modules/i18n/i18n';

import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';


const items = [
  {
    id: 1,
    content: 'Подводя итоги тестового размещения в партнерстве Omniscienta хочу отметить профессиональную аналитику удачно проведенной кампании и обоснованные рекомендации на будущее. Приятно, что Omniscienta  идет в ногу со временем и готова первой предложить клиенту прозрачную и понятную систему закупки рекламы по системе Programmatic.',
  }, {
    id: 2,
    content: 'Мы очень рады, что выбрали именно OMNIscienta в качестве подрядчика по RTB. Как результат мы получили качественный трафик и отличный сервис',
  }, {
    id: 3,
    content: 'Мы провели с OMNIscienta рекламную кампанию и обязательно продолжим сотрудничество. Для нас очень важен вопрос прозрачности цены, понимания ее формирования. OMNIscienta отвечает на этот вопрос. Кроме того, команда обеспечивает прекрасный сервис – с ними легко и приятно работать.',
  }, {
    id: 4,
    content: 'Мы всегда стараемся пробовать новые возможности и технологии. Компания Omniscienta оказалась отличным примером данной философии. Она позволила сделать рекламную кампанию высокоэффективной и обеспечила приток новых заинтересованных пользователей: при 95% новых пользователей показатель отказов составил всего 15%.',
  },

];

class OurClients extends React.Component {



  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });

  }

  render() {
    const { activeIndex } = this.state;


    const slides = items.map(item => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.id}
          src=""
        >
            <div className="row justify-content-center">
              <div className="col-xs-12 col-sm-12 col-md-8 col-lg-6 col-xl-6 text-center">

                <img className="our-clients__icon" src="/assets/icons/our_clients_1.svg" />

                <p>{item.content}</p>
              </div>
            </div>
        </CarouselItem>
      );
    });

    if(getLang() === 'en') {
      return(<div/>)
    } else {
      return (

        <div>
          <div className="our-clients">
            <div className="container">

              <div className="row">
                <div className="col text-center">
                  <h1>{t('OUR_CLIENTS_TITLE')}</h1>
                </div>
              </div>

              <Carousel
                activeIndex={activeIndex}
                next={this.next}
                previous={this.previous}
              >{slides}</Carousel>

              <div className="row justify-content-center our-clients__partners">

                <div className={`our-clients__col col-6 col-md-3 align-self-center text-center ${activeIndex === 0 ? 'active':''}`}
                     onClick={() => this.goToIndex(0)}>
                  <img style={{'max-width': '121px'}} src="/assets/images/snow-queen-white.png"/>
                </div>

                <div className={`our-clients__col col-6 col-md-3 align-self-center text-center ${activeIndex === 1 ? 'active':''}`}
                     onClick={() => this.goToIndex(1)}>
                  <img style={{'max-width': '89px'}} src="/assets/images/subaru-white.png"/>
                </div>

                <div className={`our-clients__col col-6 col-md-3 align-self-center text-center ${activeIndex === 2 ? 'active':''}`}
                     onClick={() => this.goToIndex(2)}>
                  <img style={{'max-width': '64px'}} src="/assets/images/triangle-digital-white.png"/>
                </div>

                <div className={`our-clients__col col-6 col-md-3 align-self-center text-center ${activeIndex === 3 ? 'active':''}`}
                     onClick={() => this.goToIndex(3)}>
                  <img style={{'max-width': '156px'}} src="/assets/images/media-storm-white.png"/>
                </div>

              </div>

            </div>
          </div>
          <div className="divider"/>
        </div>

      );

    }
  }
}

export default OurClients;

