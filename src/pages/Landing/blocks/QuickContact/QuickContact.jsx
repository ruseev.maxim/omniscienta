import React from 'react';

// actions
import { sendNewLead } from './../../../../actions/landingPage';

// redux
import { connect } from 'react-redux';

// prop-types
import PropTypes from 'prop-types';

// components
import QuickContactForm from './../../../../components/Forms/QuickContactForm';
import QuickContactFormWrapper from './../QuickContactFormWrapper/QuickContactFormWrapper';

import t from './../../../../modules/i18n/i18n';

let QuickContact = props => {

  const { formName } = props;

  return (
    <div>
      <div className="quick-contact">
        <div className="container">
          <div className="row">
            <div className="col">
              <h3 className="text-center">
                {t('DESCRIPTOR_DESCRIPTION')}
              </h3>
              <QuickContactFormWrapper formName={formName} />
            </div>
          </div>
        </div>
      </div>
      <div className="divider" />
    </div>
  )
};

QuickContact.propTypes = {
  formName: PropTypes.string.isRequired
};

export default QuickContact;