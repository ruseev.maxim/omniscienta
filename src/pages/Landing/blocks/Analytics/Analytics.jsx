import React from 'react';

import { connect } from 'react-redux';

import { isElementInTheCenter } from './../../../../components/modules/calculateElementPosition';

import ReactGA from 'react-ga';

import { t, getLang } from './../../../../modules/i18n/i18n';

const Analytics = props => {

  const { pageYOffset } = props;

  const triggerElement = document.querySelector('.analytics h1');
  if(triggerElement && pageYOffset) {
    if(isElementInTheCenter(triggerElement, pageYOffset)) {
      ReactGA.event({
        category: 'scroll',
        action: '3'
      });
    }
  }

  return (
    <div>
      <div className="analytics">
        <div className="container">
          <div className="row">
            <div className="col text-center">
              <h1 dangerouslySetInnerHTML={{__html: t('ANALYTICS_TITLE')}} />
              <h3 className="text-omni-gray-1">{t('ANALYTICS_DESCRIPTION')}</h3>
            </div>
          </div>


          <div className="row align-items-center analytics-content-row">

            <div className="col-xs-7 order-lg-2">
              <img className="analytics__graph"
                   src={getLang() === 'en' ? '/assets/images/analytics_en.svg' : '/assets/images/analytics.svg'}/>
            </div>

            <div className="col order-lg-1">
              <h3 dangerouslySetInnerHTML={{__html: t('ANALYTICS_LI_TITLE')}} />
              <ul className="omni-list">
                <li>{t('ANALYTICS_LI_1')}</li>
                <li>{t('ANALYTICS_LI_2')}</li>
                <li>{t('ANALYTICS_LI_3')}</li>
                <li>{t('ANALYTICS_LI_4')}</li>
              </ul>
            </div>

          </div>

        </div>

      </div>
      <div className="divider"/>
    </div>
  );
};

const mapStateToProps = state => ({
  pageYOffset: state.scroll.pageYOffset
});

export default connect(mapStateToProps)(Analytics);
