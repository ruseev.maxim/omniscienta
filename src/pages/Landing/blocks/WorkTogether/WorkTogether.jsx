import React from 'react';

import t from './../../../../modules/i18n/i18n';

// components
import CallToActionFormWrapper from './../CallToActionFormWrapper/CallToActionFormWrapper';

let WorkTogether = () => {

  return(
    <div>
      <div className="work-together">
        <div className="container">
          <div className="row">
            <div className="col">
              <h1 className="text-center">{t('WORK_TOGETHER_TITLE')}</h1>
            </div>
          </div>

          <div className="row justify-content-center">
            <div className="col-sm-8 col-xs-12 col-md-6 col-lg-4">
              <CallToActionFormWrapper formName="workTogether" />
            </div>
          </div>

        </div>
      </div>
      <div className="divider" />
    </div>

  )
};

export default WorkTogether;