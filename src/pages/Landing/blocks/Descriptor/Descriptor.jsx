import React from 'react';

// form
import QuickContactFormWrapper from './../QuickContactFormWrapper/QuickContactFormWrapper';

import particlesConfig from './particlesConfig';

import t from './../../../../modules/i18n/i18n';

class Descriptor extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    particlesJS('particles-js', particlesConfig);
  }

  render() {

    return(
      <div>
        <div className="descriptor">
          <div className="container">
            <div className="row">
              <div style={{'zIndex': 5}} className="col text-center">
                <h2 dangerouslySetInnerHTML={{__html: t('DESCRIPTOR_TITLE')}} />
                <h3 className="text-omni-gray-1">
                  {t('DESCRIPTOR_DESCRIPTION')}
                </h3>
              </div>
            </div>

            <div className="row">
              <div style={{'zIndex': 5}} className="col">
                <QuickContactFormWrapper formName="descriptorBlock" />
              </div>
            </div>


          </div>
          <div id="particles-js" />
        </div>
        <div className="divider" />
      </div>
    )

  }
}

export default Descriptor;
