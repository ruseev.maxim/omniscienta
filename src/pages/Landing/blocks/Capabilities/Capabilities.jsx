import React from 'react';

import { connect } from 'react-redux';

import { isElementInTheCenter } from './../../../../components/modules/calculateElementPosition';

import ReactGA from 'react-ga';

import t from './../../../../modules/i18n/i18n';

const Capabilities = props => {

  const { pageYOffset } = props;

  const triggerElement = document.querySelector('.capabilities h1');
  if(triggerElement && pageYOffset) {
    if(isElementInTheCenter(triggerElement, pageYOffset)) {
      ReactGA.event({
        category: 'scroll',
        action: '5'
      });
    }
  }

  return (
    <div>
      <div className="capabilities">
        <div className="container">

          <div className="row">
            <div className="col">
              <h1 className="text-center">{t('FEATURES_TITLE')}</h1>
            </div>
          </div>

          <div className="row">

            <div className="capabilities__col col-lg-4 col-md-6 col-sm-6 col-xs-12 text-center">
              <img className="capabilities__icon" src="/assets/icons/capabilities_1.svg"/>
              <h3 dangerouslySetInnerHTML={{__html: t('FEATURES_LP_DESC')}} />
              <p dangerouslySetInnerHTML={{__html: t('FEATURES_LP_DESC')}} />
            </div>

            <div className="capabilities__col col-lg-4 col-md-6 col-sm-6 col-xs-12 text-center">
              <img className="capabilities__icon" src="/assets/icons/capabilities_2.svg"/>
              <h3>{t('FEATURES_SALE_TITLE')}</h3>
              <p dangerouslySetInnerHTML={{__html: t('FEATURES_SALE_DESC')}} />
            </div>

            <div className="capabilities__col col-lg-4 col-md-6 col-sm-6 col-xs-12 text-center">
              <img className="capabilities__icon" src="/assets/icons/capabilities_3.svg"/>
              <h3>{t('FEATURES_POST_TITLE')}</h3>
              <p dangerouslySetInnerHTML={{__html: t('FEATURES_POST_DESC')}} />
            </div>

            <div className="capabilities__col col-lg-4 col-md-6 col-sm-6 col-xs-12 text-center">
              <img className="capabilities__icon" src="/assets/icons/capabilities_4.svg"/>
              <h3>{t('FEATURES_INFO_TITLE')}</h3>
              <p dangerouslySetInnerHTML={{__html: t('FEATURES_INFO_DESC')}} />
            </div>

            <div className="capabilities__col col-lg-4 col-md-6 col-sm-6 col-xs-12 text-center">
              <img className="capabilities__icon" src="/assets/icons/capabilities_5.svg"/>
              <h3>{t('FEATURES_TRADE_TITLE')}</h3>
              <p dangerouslySetInnerHTML={{__html: t('FEATURES_TRADE_DESC')}} />
            </div>

            <div className="capabilities__col col-lg-4 col-md-6 col-sm-6 col-xs-12 text-center">
              <img className="capabilities__icon" src="/assets/icons/capabilities_6.svg"/>
              <h3>{t('FEATURES_OPEN_SEGMENT_TITLE')}</h3>
              <p dangerouslySetInnerHTML={{__html: t('FEATURES_OPEN_SEGMENT_DESC')}} />
            </div>


          </div>
        </div>
      </div>
      <div className="divider"/>
    </div>
  );
};

const mapStateToProps = state => ({
  pageYOffset: state.scroll.pageYOffset
});

export default connect(mapStateToProps)(Capabilities);
