import { all } from 'redux-saga/effects';

import { watchSendNewLead } from './landingPage';

export default function* rootSaga() {
  yield all([
    watchSendNewLead(),
  ]);
}