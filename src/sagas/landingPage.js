import axios from 'axios';
import ReactGA from 'react-ga';

// saga
import { put, takeEvery } from 'redux-saga/effects';

// actions
import { showAlert } from './../actions/alert';
import { hideGetAuditDialog } from './../actions/getAudit';

function* sendNewLead({payload: {resolve, reject, values}}) {

  try {
    const API = '/mailer.php';
    yield axios.post(API, {
      name: values.name,
      phone: values.phone,
      email: values.email,
    });

    // show alert after successful request
    yield put(
      showAlert('Ваша заявка принята', 'Наш менеджер свяжется с Вами в ближайшее время', 'ok')
    );

    ReactGA.event({
      category: 'request',
      action: 'send'
    });

    // hide get audit dialog if there is one
    yield put(hideGetAuditDialog());

    yield resolve();
  } catch(e) {
    reject();
  }
}

function* watchSendNewLead() {
  yield takeEvery('SEND_NEW_LEAD', sendNewLead);
}

export {
  watchSendNewLead,
}
