import i18nContainer from './i18nContainer';


function t(text) {
  let lang = getLang();
  return i18nContainer[text][lang ? lang : 'ru'];
}

function link(link) {
  let lang = getLang();

  return `${lang ? '/'+lang : ''}${link}`;
}

function getLang() {
  const firstPathItem = window.location.pathname.split('/')[1];
  let lang;
  switch(firstPathItem) {
    case 'en':
      lang = 'en';
      break;
    case 'ru':
      lang = 'ru';
      break;
    default:
      lang = null;
      break;
  }
  return lang;
}

function changeLang(lang) {
  const firstPathItem = window.location.pathname.split('/')[1];

}

export {
  link,
  t,
  getLang,
};
export default t;