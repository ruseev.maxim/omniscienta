const i18nContainer = {
  REQUIRED_PHONE_OR_EMAIL: {
    ru: 'Введите номер или e-mail',
    en: 'Type phone or e-mail',
  },
  INVALID_EMAIL: {
    ru: '',
    en: '',
  },
  BUTTON_GET_AUDIT: {
    ru: 'Получить аудит',
    en: 'Get audit',
  },
  TOOLBAR_LINK_HOME: {
    ru: 'Главная',
    en: 'Home',
  },
  TOOLBAR_LINK_ESTATE: {
    ru: 'Решения для недвижимости',
    en: 'Estate',
  },
  FORM_NAME_PLACEHOLDER: {
    ru: 'Введите имя:',
    en: 'Name:',
  },
  FORM_EMAIL_PLACEHOLDER: {
    ru: 'Введите e-mail:',
    en: 'E-mail:',
  },
  FORM_PHONE_PLACEHOLDER: {
    ru: 'Введите телефон:',
    en: 'Phone:',
  },
  FORM_PHONE_OR_EMAIL_PLACEHOLDER: {
    ru: 'Введите номер или e-mail:',
    en: 'Phone or e-mail:',
  },

  DESCRIPTOR_TITLE: {
    ru: 'Digital агентство с технологиями Big&nbsp;Data &mdash; DSP&nbsp;&&nbsp;DMP',
    en: 'Digital agency with Big&nbsp;Data technologies &mdash; DSP&nbsp;&&nbsp;DMP',
  },
  DESCRIPTOR_DESCRIPTION: {
    ru: 'Получите аудит сайта и рекомендации от Omniscienta бесплатно',
    en: 'Get web-site audit and recommendations from Omniscienta for free',
  },
  TECHNOLOGIES_TITLE: {
    ru: 'Ведение рекламных кампаний<br>с использованием технологии DMP',
    en: 'Running advertising campaigns<br>with DMP technology',
  },
  TECHNOLOGIES_DESCRIPTION: {
    ru: 'Разработка Digital стратегии',
    en: 'Digital strategy development',
  },
  TECHNOLOGIES_PROGRAMMATIC: {
    ru: 'Собственная технология алгоритмических<br>закупок рекламы для всех устройств. Познакомьте<br>холодную аудиторию с вашим брендом.',
    en: 'Own algorithmic buying technology<br>for all devices.<br>Introduce your brand to the audience',
  },
  TECHNOLOGIES_SOCIAL_MARKETING: {
    ru: 'Расскажите о вашем предложении<br>заинтересованной аудитории',
    en: 'Tell your story to people',
  },
  TECHNOLOGIES_MARKETING_DATA_PLATFORM: {
    ru: 'MDP – подраздел DMP. Платформа по управлению<br>данными с помощью Data Science. Максимальная<br>персонализация рекламы под каждого<br>пользователя в любом рекламном канале.',
    en: 'MDP is a part of DMP for data management.<br>Maximum personalisation of advertising<br>for any user for any channel.',
  },
  TECHNOLOGIES_CONTEXT_TITLE: {
    ru: 'Контекстная реклама',
    en: 'Context',
  },
  TECHNOLOGIES_CONTEXT_DESCRIPTION: {
    ru: 'Отвечайте пользователям на их запросы',
    en: 'Answer on users’ inquiries',
  },
  TECHNOLOGIES_RETARGETING_TITLE: {
    ru: 'Ретаргетинг и e-mail marketing',
    en: 'Retargeting and mail-marketing',
  },
  TECHNOLOGIES_RETARGETING_DESCRIPTION: {
    ru: 'Возвращайте клиентов для повторных<br>покупок',
    en: 'Return your clients for repeat purchases'
  },
  TECHNOLOGIES_TAKE_A_LOOK_TITLE: {
    ru: 'Взгляд изнутри',
    en: 'Take a look inside',
  },
  TECHNOLOGIES_TAKE_A_LOOK_DESCRIPTION: {
    ru: 'Посмотрите как мы работаем',
    en: 'Video'
  },
  ANALYTICS_TITLE: {
    ru: 'Кросс-канальная аналитика<br/>с помощью Data Science',
    en: 'Omnichannel analytics based<br>on Data Science',
  },
  ANALYTICS_DESCRIPTION: {
    ru: 'Определим эффективность совместного использования каналов',
    en: '',
  },
  ANALYTICS_LI_TITLE: {
    ru: 'С помощью Data Science:',
    en: 'Data science:',
  },
  ANALYTICS_LI_1: {
    ru: 'Определяем зависимость каналов между собой',
    en: 'Determines dependences between channels',
  },
  ANALYTICS_LI_2: {
    ru: 'Определяем значимость каждого канала на общий результат',
    en: 'Determines significance of channel in the result',
  },
  ANALYTICS_LI_3: {
    ru: 'Эффективно перераспределяем бюджеты на кампании, которые в общей цепочке дают необходимые CPO, CPA, звонки, заявки',
    en: 'Efficiently allocates advertising budget to reach KPI’s',
  },
  ANALYTICS_LI_4: {
    ru: 'Прогнозируем CPO, CPA на последующие периоды',
    en: 'Forecasts CPO and CPA',
  },
  PRICES_TITLE: {
    ru: 'Ценообразование',
    en: 'Pricing',
  },
  PRICES_DESCRIPTION: {
    ru: 'Открытое ценообразование – позволяет выкупать более дорогой и эффективный инвентарь у паблишеров',
    en: 'Open pricing allows to buy more expense and efficient inventory',
  },
  PRICE_BASE_AMOUNT: {
    ru: 'от 250 000 р/мес',
    en: 'from 10 000$ p.m.',
  },
  PRICE_EXPAND_AMOUNT: {
    ru: 'от 500 000 р/мес',
    en: 'from 20 000$ p.m.',
  },
  PRICE_ADVANCED_AMOUNT: {
    ru: 'от 750 000 р/мес',
    en: 'from 30 000$ p.m.',
  },
  PRICE_MAX_AMOUNT: {
    ru: 'от 1 000 000 р/мес',
    en: 'from 40 000$ p.m.',
  },
  PRICES_BASE: {
    ru: 'Базовый',
    en: 'Base',
  },
  PRICES_EXPAND: {
    ru: 'Расширенный',
    en: 'Expand',
  },
  PRICES_ADVANCED: {
    ru: 'Продвинутый',
    en: 'Advanced',
  },
  PRICES_MAX: {
    ru: 'Максимальный',
    en: 'Maximum',
  },
  PRICES_BUDGET : {
    ru: 'Бюджет',
    en: 'Budget',
  },
  PRICES_CHANNELS : {
    ru: 'Рекламные каналы',
    en: 'Channels',
  },
  PRICES_COMMISSION : {
    ru: 'Комиссия за реализацию',
    en: 'Commission',
  },
  PRICES_ANALYTICS : {
    ru: 'Кросс-канальная аналитика',
    en: 'Cross-channel analytics',
  },
  PRICES_PERSONALISATION : {
    ru: 'Персонализация с помощью DMP',
    en: 'Personalisation with DMP',
  },
  PRICES_COURSE : {
    ru: 'Обучение кросс-канальной аналитике',
    en: 'Cross-channel analytics course',
  },
  PRICES_MANAGER : {
    ru: 'Персональный Account Manager',
    en: 'Personal account manager',
  },
  PRICES_INFO : {
    ru: 'Открытая информация по источникам',
    en: 'Source information',
  },
  PRICES_CHOOSE_BUTTON: {
    ru: 'Выбрать',
    en: 'Choose',
  },
  PRICES_CHOOSE_BASE: {
    ru: 'Тариф &mdash; Базовый',
    en: 'Base',
  },
  PRICES_CHOOSE_EXPAND: {
    ru: 'Тариф &mdash; Расширенный',
    en: 'Expand',
  },
  PRICES_CHOOSE_ADVANCED: {
    ru: 'Тариф &mdash; Продвинутый',
    en: 'Advanced',
  },
  PRICES_CHOOSE_MAX: {
    ru: 'Тариф &mdash; Максимальный',
    en: 'Maximum',
  },
  FEATURES_TITLE: {
    ru: 'Дополнительные возможности',
    en: 'Advanced features',
  },
  FEATURES_LP_TITLE: {
    ru: 'DMP + Лендинги',
    en: 'DMP+Landing page',
  },
  FEATURES_LP_DESC: {
    ru: 'Персонализация сайта под<br>сегменты пользователей',
    en: 'Personalisation of website<br>for users segments',
  },
  FEATURES_SALE_TITLE: {
    ru: 'DMP + Скрипты продаж',
    en: 'DMP + sale-scripts',
  },
  FEATURES_SALE_DESC: {
    ru: 'Менеджер по продажам знает,<br>кто заранее обратился в компанию<br>и предлагает нужные продукты',
    en: 'Sales manager knows in advance<br>who will call and offers products needed',
  },
  FEATURES_POST_TITLE: {
    ru: 'Post-Cliсk, Post-View отчеты',
    en: 'Post-Click, Post-View report',
  },
  FEATURES_POST_DESC: {
    ru: 'Отчеты с различными<br>видами атрибуции',
    en: 'Reports with<br>different attributives',
  },
  FEATURES_INFO_TITLE: {
    ru: 'Открытые источники инвентаря',
    en: 'Open inventory info',
  },
  FEATURES_INFO_DESC: {
    ru: 'Отображаем всю информацию<br>о SSP и площадках',
    en: 'Shows information<br>about SSP and publishers',
  },
  FEATURES_TRADE_TITLE: {
    ru: 'Trading-Desk',
    en: 'Trading-Desk',
  },
  FEATURES_TRADE_DESC: {
    ru: 'Real-time статистика<br>по кампаниям',
    en: 'Real-time campaign<br>statistics',
  },
  FEATURES_OPEN_SEGMENT_TITLE: {
    ru: 'Открытые источники данных',
    en: 'Open segments info',
  },
  FEATURES_OPEN_SEGMENT_DESC: {
    ru: 'Полная информация о сегментах<br>(Interest, LAL, Retargeting)',
    en: 'Full info about segments<br>(interests, LAL, Retargeting)',
  },
  WORK_TOGETHER_TITLE: {
    ru: 'Начнем работать вместе',
    en: 'Let’s start working!',
  },
  OUR_CLIENTS_TITLE: {
    ru: 'С нами сотрудничают',
    en: 'Our clients',
  }
};

export default i18nContainer;