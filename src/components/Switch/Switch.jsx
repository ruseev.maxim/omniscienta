import React from 'react';

class Switch extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div className={`switch ${true ? 'switch--active' : ''}`}>
        <div className="switch__bar" />
        <div className="switch__thumb" />
      </div>
    )
  }
}

export default Switch;