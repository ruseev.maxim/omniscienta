import React from 'react';

import { connect } from 'react-redux';

import { BrowserRouter as Router, Route, Link, withRouter } from 'react-router-dom';

import Alert from './../Alert/Alert';
import GetAudit from './../GetAudit/GetAudit';

import Landing from './../../pages/Landing/Landing';
import Estate from './../../pages/Estate/Estate';

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { onScroll } = this.props;

    window.addEventListener('scroll', e => {
      onScroll(window.pageYOffset);
    })
  }

  render() {
    return(
      <div>
        <Alert/>
        <GetAudit/>
        <Route exact={true} path="/" component={Landing} />
        <Route exact={true} path="/estate" component={Estate} />
        <Route exact={true} path="/en/estate" component={Estate}/>
        <Route exact={true} path="/en" component={Landing}/>
        <Route exact={true} path="/ru/estate" component={Estate}/>
        <Route exact={true} path="/ru" component={Landing}/>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onScroll: (pageYOffset) => dispatch({type: 'WINDOW_SCROLLED', pageYOffset}),
});

export default withRouter(connect(null, mapDispatchToProps)(App));