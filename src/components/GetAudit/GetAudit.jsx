import React from 'react';

// prop-types
import PropTypes from 'prop-types';

// redux
import { connect } from 'react-redux';

// components
import { Button, Modal } from 'reactstrap';

// actions
import { hideGetAuditDialog } from './../../actions/getAudit';

// form
import CallToActionFormWrapper from './../../pages/Landing/blocks/CallToActionFormWrapper/CallToActionFormWrapper';

import t from './../../modules/i18n/i18n';

let GetAudit = props => {

  const { getAudit, closeDialog } = props;

  let title = t('DESCRIPTOR_DESCRIPTION');

  if(getAudit.title) title = getAudit.title;

  return (
    <div>
      <Modal isOpen={getAudit.show}>
        <div className="omni-modal__close" onClick={closeDialog} >
          <img src="/assets/icons/close.svg" />
        </div>
        <div className="omni-modal">

          <div className="omni-modal__title">
            <h3 className="text-center" dangerouslySetInnerHTML={{__html: title}} />
          </div>

          {getAudit.description ?
            <div className="omni-modal__description text-center">
              <p dangerouslySetInnerHTML={{__html: getAudit.description}} />
            </div> : null}


          <div className="omni-modal__content text-center">
            <CallToActionFormWrapper formName="getAuditDialog" />
          </div>

        </div>
      </Modal>
    </div>
    );
};


const mapStateToProps = state => ({
  getAudit: state.getAudit,
});

const mapDispatchToProps = dispatch => ({
  closeDialog: () => dispatch(hideGetAuditDialog()),
});

GetAudit.PropTypes = {
  getAudit: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(GetAudit);
