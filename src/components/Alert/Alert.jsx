import React from 'react';

// prop-types
import PropTypes from 'prop-types';

// redux
import { connect } from 'react-redux';
import { hideAlert } from './../../actions/alert';
import { Button, Modal } from 'reactstrap';


let Alert = props => {
  const { alert, hideAlert } = props;

  return (
    <div>
      <Modal isOpen={alert.show}>
        <div className="omni-modal">
          <div className="omni-modal__title">
            <h3 className="text-center">{alert.title}</h3>
          </div>

          <div className="omni-modal__content text-center">{alert.content}</div>

          <div className="omni-modal__actions text-center">
            <Button color="primary" onClick={hideAlert}>{alert.ok}</Button>
          </div>
        </div>
      </Modal>
    </div>
  )
};

const mapStateToProps = state => {
  return {
    alert: state.alert,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    hideAlert: () => dispatch(hideAlert())
  }
};

Alert.PropTypes = {
  alert: PropTypes.object.isRequired,
};


export default connect(mapStateToProps, mapDispatchToProps)(Alert);