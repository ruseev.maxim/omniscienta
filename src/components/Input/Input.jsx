import React from 'react';

import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

const Input = ({
                 className,
                 formGroupClassName,
                 input,
                 label,
                 type,
                 style,
                 placeholder,
                 meta: {touched, error}
               }) => {


  // error message
  const errorContainer =
    <div className="form-group-errors">
      <div className="form-group-errors__error">{error}</div>
      <div className="form-group-errors__triangle"/>
    </div>;

  // exclamation mark
  const exclamationMark =
    <div className="form-group__exclamation-mark">
      <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M12 24C5.373 24 0 18.626 0 12S5.374 0 12 0s12 5.374 12 12-5.374 12-12 12zm2-18.667c0-1.208-.79-2-2-2s-2 .792-2 2v7.334c0 1.208.79 2 2 2s2-.792 2-2V5.333zm0 13.334c0-1.21-.79-2-2-2s-2 .79-2 2c0 1.208.79 2 2 2s2-.79 2-2z"
          fillRule="nonzero" fill="#ED4760"/>
      </svg>
    </div>;

  return (
    <div className={`form-group ${formGroupClassName || ''}`}>
      {label ? <label>label</label> : null}
      <input style={style} {...input} placeholder={placeholder} type={type}
             className={`form-control ${(touched && error) ? 'is-invalid' : null} ${className}`}/>

      {/*<ReactCSSTransitionGroup*/}
        {/*transitionName="example"*/}
      {/*>*/}

        {(touched && error) ? exclamationMark : null}
      {/*</ReactCSSTransitionGroup>*/}



      {/*<ReactCSSTransitionGroup*/}
        {/*transitionName="example"*/}
        {/*>*/}

        {(touched && error) ? errorContainer : null}
      {/*</ReactCSSTransitionGroup>*/}

    </div>
  )


};

export default Input;