import React from 'react';

// prop-types
import PropTypes from 'prop-types';

// redux forms
import { Field, reduxForm } from 'redux-form';

// validators
import { required, email } from './../modules/inputValidetions';

// bootstrap
import { Button } from 'reactstrap'
import Input from './../Input/Input';

// redux
import { compose } from 'redux';
import { connect } from 'react-redux';

import t from './../../modules/i18n/i18n';

// reset from after successful submitting
const onSubmitSuccess = (result, dispatch, props) => {
  props.reset();
};

let CallToActionForm = props => {
  const {handleSubmit, pristine, submitting, invalid} = props;

  return (
    <form className="row call-to-action-form justify-content-center" onSubmit={handleSubmit}>
      <div className="col">
        <Field name="name" component={Input} type="text" validate={[required]}
               placeholder={t('FORM_NAME_PLACEHOLDER')}/>
      </div>

      <div className="w-100"/>

      <div className="col">
        <Field name="email" component={Input} type="text" validate={[required]}
               placeholder={t('FORM_EMAIL_PLACEHOLDER')}/>
      </div>

      <div className="w-100"/>

      <div className="col">
        <Field style={{'margin-bottom': '40px'}} name="phone" component={Input} type="text" validate={[required]}
               placeholder={t('FORM_PHONE_PLACEHOLDER')}/>
      </div>

      <div className="w-100"/>

      <div className="col text-center">
        <Button color="primary" type="submit" disabled={pristine || submitting || invalid}>
          {t('BUTTON_GET_AUDIT')}
        </Button>
      </div>

    </form>
  );
};

// export default CallToActionForm = reduxForm({
//   form: 'callToActionForm',
// })(CallToActionForm);

CallToActionForm.propTypes = {
  formName: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const mapStateToProps = (state, props) => ({
  form: props.formName,
  onSubmitSuccess,
});

export default compose(
  connect(mapStateToProps),
  reduxForm()
)(CallToActionForm);