const isElementInTheCenter = (elem, pageYOffset) => {
  const windowHeight = window.innerHeight;
  const elemPosition = Math.floor(elem.getBoundingClientRect().top + pageYOffset);

  const center = elemPosition - pageYOffset - windowHeight / 2;
  return (center <= 20 && center >= -20);
};

export {
  isElementInTheCenter
};