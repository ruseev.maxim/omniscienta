import t from './../../modules/i18n/i18n';

const emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const defaultErrorMessage = 'Это поле обязательно';

export const required = value => value ? undefined : t('REQUIRED_PHONE_OR_EMAIL');

export const email = value => emailPattern.test(value) ? undefined : 'Not an email';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
export default (async function showResults(values) {
  await sleep(500); // simulate server latency
  window.alert(`You submitted:\n\n${JSON.stringify(values, null, 2)}`);
});
