const initialState = {
  pageYOffset: 0,
};

export default function scroll(state = initialState, action) {
  switch(action.type) {
    case 'WINDOW_SCROLLED':
      return Object.assign({}, state, {
        pageYOffset: action.pageYOffset
      });
    default: return state;
  }
}