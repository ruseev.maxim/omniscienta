import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

// reducers
import alert from './alert';
import getAudit from './getAudit';
import scroll from './scroll';

const rootReducer = combineReducers({
  form: formReducer,
  alert,
  getAudit,
  scroll,
});

export default rootReducer;