const initialState = {
  show: false,
};

export default function getAudit(state = initialState, action) {
  switch(action.type) {
    case 'SHOW_GET_AUDIT_DIALOG':
      return Object.assign({}, state, {
        show: true,
        title: action.title,
        description: action.description,
      });
    case 'HIDE_GET_AUDIT_DIALOG':
      return Object.assign({}, state, {
        show: false,
        title: '',
        description: '',
      });

    default: return state;
  }
}