const initialState = {
  show: false,
  title: '',
  content: '',
  ok: '',
};

export default function alert(state = initialState, action) {
  switch(action.type) {

    case 'SHOW_ALERT':
      return Object.assign({}, state, {
        show: true,
        title: action.title,
        content: action.content,
        ok: action.ok,
      });

    case 'HIDE_ALERT':
      return Object.assign({}, state, {
        show: false,
        title: '',
        content: '',
        ok: '',
      });

    default: return state;
  }
}