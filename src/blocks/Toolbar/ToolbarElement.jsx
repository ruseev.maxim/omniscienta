import React from 'react';
import PropTypes from 'prop-types';

const ToolbarElement = ({name}) => (
  <div className="toolbar__menu-element row align-items-center">
    <div>{name}</div>
  </div>
);

ToolbarElement.propTypes = {
  name: PropTypes.string.isRequired,
};

export default ToolbarElement