import React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import Switch from './../../components/Switch/Switch';

import { link, t } from './../../modules/i18n/i18n';
import {getLang} from "../../modules/i18n/i18n";

class LanguageDropdown extends React.Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.changeLang = this.changeLang.bind(this);

    this.state = {
      dropdownOpen: false,
      lang: getLang()
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  changeLang() {
    let link = `/${this.props.lang}`;
    const pathItems = window.location.pathname.split('/');
    console.log(pathItems);
    if(pathItems[1] !== 'ru' || pathItems[1] !== 'en') {
      for(let i = 2; i < pathItems.length; i++) {
        if(pathItems[i]) {
          link += `/${pathItems[i]}`;
        }
      }
    }
    window.location.href = link;
  }

  render() {
    let langIcon = <img width="20px" height="15px" src={`/assets/icons/flag_${this.props.lang}.svg`}/>;

    return(
      <div className="language-dropdown" onClick={this.changeLang}>
        <div className="language-dropdown__button">
          {langIcon}
        </div>
        {/*<div className="language-dropdown__content">*/}
          {/*<img style={{marginRight: '26px', display: 'inline'}} src="/assets/icons/flag.svg" alt=""/>*/}
          {/*<Switch/>*/}
        {/*</div>*/}

      </div>
    )
  }
}

export default LanguageDropdown;