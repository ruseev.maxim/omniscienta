// react
import React from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

// redux
import { connect } from 'react-redux';

// actions
import { showGetAuditDialog } from './../../actions/getAudit';

// components
import { Button } from 'reactstrap';
import Switch from './../../components/Switch/Switch';
import LanguageDropdown from './LanguageDropdown';
import { link, t, getLang } from './../../modules/i18n/i18n';

class Toolbar extends React.Component {

  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: false,
    };
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render() {

    const { showGetAuditDialog } = this.props;

    return (

    <Navbar className="fixed-top">
      <NavbarBrand href={link('/')} className="mr-auto">
        <img src="/assets/images/logo.svg" />
      </NavbarBrand>

      <NavbarToggler onClick={this.toggleNavbar} className="mr-2 d-sm-block d-xs-block d-md-block d-lg-block d-xl-none">
        <img src="/assets/icons/burger.svg" />
      </NavbarToggler>

      <Nav className="d-none d-xl-flex col" style={{'margin-left': '28px'}}>

        <NavItem>
          <Link to={link('/')} className="navbar-menu-element nav-link">
            {t('TOOLBAR_LINK_HOME')}
          </Link>
        </NavItem>

        {getLang() !== 'en' ?
          <NavItem>
            <Link to={link('/estate')} className="navbar-menu-element nav-link">
              {t('TOOLBAR_LINK_ESTATE')}
            </Link>
          </NavItem>
          : null
        }


        <div className="col"></div>
        <NavItem style={{padding: '10px'}}>
          <a href="https://www.facebook.com/omniscientadsp/" target="_blank" className="language-dropdown">
            <div className="language-dropdown__button">
              <img width="20px" height="15px" src={`/assets/icons/group-8.svg`}/>
            </div>
          </a>
        </NavItem>
        <NavItem style={{padding: '10px'}}>
          <LanguageDropdown lang="ru" />
        </NavItem>
        <NavItem style={{padding: '10px'}}>
          <LanguageDropdown lang="en" />
        </NavItem>

        <NavItem>
          <strong className="toolbar__phone nav-link">+7 (495) 369 0958</strong>
        </NavItem>

        <NavItem>
          <Button style={{'margin-top': '11px'}} color="primary" onClick={showGetAuditDialog}>{t('BUTTON_GET_AUDIT')}</Button>
        </NavItem>
      </Nav>

      <Collapse isOpen={this.state.collapsed} navbar>
        <Nav className="ml-auto" navbar>

          <NavItem>
            <Link to="/" className="navbar-menu-element nav-link">
              {t('TOOLBAR_LINK_HOME')}
            </Link>
          </NavItem>

          {getLang() !== 'en' ?
            <NavItem>
              <Link to="/estate" className="navbar-menu-element nav-link">
                {t('TOOLBAR_LINK_ESTATE')}
              </Link>
            </NavItem>
            : null
          }

          <div style={{'margin': '28px 0'}} className="divider" />

          <NavItem className="row justify-content-center">
            <div className="col-auto">
              <a href="https://www.facebook.com/omniscientadsp/" target="_blank" className="language-dropdown">
                <div className="language-dropdown__button">
                  <img width="20px" height="15px" src={`/assets/icons/group-8.svg`}/>
                </div>
              </a>

            </div>
            <div className="col-auto">
              <LanguageDropdown lang="ru" />
            </div>
            <div className="col-auto">
              <LanguageDropdown lang="en" />
            </div>

          </NavItem>

          <NavItem className="text-center">
            <div className="toolbar__phone nav-link ">+7 (495) 369 0958</div>
          </NavItem>

          <NavItem className="text-center">
            <Button className="toolbar__call-to-action" color="primary" onClick={showGetAuditDialog}>{t('BUTTON_GET_AUDIT')}</Button>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
    )

  }
}

const mapDispatchToProps = dispatch => ({
  showGetAuditDialog: () => dispatch(showGetAuditDialog()),
});

export default connect(null, mapDispatchToProps)(Toolbar);
