import React from 'react';

const Footer = () => (
  <div className="footer">
    <div className="container">
      <div className="row align-items-center">
        <div className="footer-col col-xs-12 col-sm-12 col-md-4">Оmniscienta &copy; 2017</div>
        <div className="footer-col col-xs-12 col-sm-12 col-md-4">Политика конфиденциальности</div>
        <div className="footer-col col-xs-12 col-sm-12 col-md-4"></div>
      </div>
    </div>
  </div>
);

export default Footer;
