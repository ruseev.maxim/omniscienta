import React from 'react';
import ReactDOM from 'react-dom';
import ReactGA from 'react-ga';
import { BrowserRouter as Router } from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';
ReactGA.initialize('UA-108227054-1');

import App from './components/App/App';

function logPageView() {
  ReactGA.set({ page: window.location.pathname + window.location.search });
  ReactGA.pageview(window.location.pathname + window.location.search);
}

import './assets/styles/scss/bootstrap.scss';

const Root = () => {
  return (
    <Router>
      <Provider store={store}>
        <App />
      </Provider>
    </Router>


  )
};

ReactDOM.render(<Root />, document.getElementById('root'));
